class WaveformRepository:
    """Stores a loaded waveform to be queried later."""
    def __init__(self, data_loader, validator, frequency):
        self.__validator = validator
        self.__frequency = frequency
        self.__min_time = 0
        self.__max_time = 0

        chunks = data_loader.load()
        self.__set_data(chunks)

    def __set_data(self, chunks):
        """Saves the array of data for later retrieval"""
        chunks.sort(key=lambda c: int(c['time']))
        self.__min_time = chunks[0]['time']
        self.__max_time = chunks[len(chunks)-1]['time']

        self.__initialize_data_array()

        for chunk in chunks:
            self.__add(chunk)

    def __initialize_data_array(self):
        """Initializes the internal data array to allow for data to be saved"""
        seconds = (self.__max_time - self.__min_time) / 1000 + 1
        size = int(seconds * self.__frequency)
        self.__data = [None] * size

    def __add(self, chunk):
        """Validates a chunk and adds its values to the internal data array"""
        if self.__validator.validate_chunk(chunk):
            offset = int((chunk['time'] - self.__min_time) / 1000 * self.__frequency)

            for i in range(0, self.__frequency):
                self.__data[offset + i] = chunk['values'][i]

        return True

    def get_range(self, hour_offset, duration_in_minutes):
        """Returns an array of data starting at the specified offset and extending for the specified duration"""
        start, end = self.__get_range_indices(hour_offset, duration_in_minutes)

        return self.__data[start:end]

    def get_timestamps(self, hour_offset, duration_in_minutes):
        """Returns an array of timestamps starting at the specified offset and extending for the specified duration"""
        start, end = self.__get_range_indices(hour_offset, duration_in_minutes)
        return [t/240/60/60 for t in range(start, end)]

    def __get_range_indices(self, hour_offset, duration_in_minutes):
        """Calculates the start and end indices within the array starting at the specified offset and extending for the specified duration"""
        start = int(hour_offset * 60 * 60 * self.__frequency)
        end = int(start + duration_in_minutes * 60 * self.__frequency)

        count = len(self.__data)
        if start >= count:
            return []

        end = end if end < count else count

        return start, end
