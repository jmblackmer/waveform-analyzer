import unittest
import numpy
from DataValidator import DataValidator

int16 = numpy.iinfo(numpy.int16)
data_validator = DataValidator(int16.min, int16.max, 240)


class TestDataValidator(unittest.TestCase):
    def test_validate_chunk(self):
        self.assertFalse(data_validator.validate_chunk({
            "time": 0,
            "values": range(0, 239)
        }))
        self.assertFalse(data_validator.validate_chunk({
            "time": 0,
            "values": [999999 for r in range(0, 240)]
        }))
        self.assertTrue(data_validator.validate_chunk({
            "time": 0,
            "values": range(0, 240)
        }))

    def test_validate_value(self):
        self.assertFalse(data_validator.validate_value(999999))
        self.assertTrue(int16.max)
        self.assertTrue(int16.min)

