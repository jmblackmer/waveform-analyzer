import unittest
from ConfigurationManager import ConfigurationManager

config_path = "../config.json"


class TestConfigurationManager(unittest.TestCase):
    def test_load(self):
        config = ConfigurationManager.load(config_path)
        self.assertIsNotNone(config)
        self.assertTrue("dataPath" in config)

    def test_get_value(self):
        config = ConfigurationManager(config_path)
        self.assertIsNotNone(config.get_value("dataPath"))
        self.assertEqual(config.get_value("foo", 100), 100)
