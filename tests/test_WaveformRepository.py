import unittest
from WaveformRepository import WaveformRepository


class TestDataLoader:
    def load(self):
        return [
            {"time": second * 1000, "values": range(0, 240)} for second in range(0, 60)
        ]


class TestDataValidator:
    def __init__(self, value):
        self.run = 0
        self.__value = value

    def validate_chunk(self, chunk):
        self.run += 1
        return self.__value


class TestWaveformRepository(unittest.TestCase):
    def test_get_range(self):
        validator = TestDataValidator(True)
        repository = WaveformRepository(TestDataLoader(), validator, 240)
        self.assertEqual(validator.run, 60)
        data = [d for d in repository.get_range(0, 1) if d is not None]
        self.assertEqual(len(data), 60*240)

        validator = TestDataValidator(False)
        repository = WaveformRepository(TestDataLoader(), validator, 240)
        self.assertEqual(validator.run, 60)
        data = [d for d in repository.get_range(0, 1) if d is not None]
        self.assertEqual(len(data), 0)

    def test_get_timestamps(self):
        validator = TestDataValidator(False)
        repository = WaveformRepository(TestDataLoader(), validator, 240)
        self.assertEqual(validator.run, 60)
        data = [d for d in repository.get_timestamps(0, 1) if d is not None]
        self.assertListEqual(data, [t/240/60/60 for t in range(0, 60*240)])
