import unittest
from DataLoader import FilePathDataLoader

data_loader = FilePathDataLoader("../data/*.txt.gz")


class TestDataLoader(unittest.TestCase):
    def test_load(self):
        data = data_loader.load()
        self.assertIsNotNone(data)
        self.assertGreater(len(data), 0)
