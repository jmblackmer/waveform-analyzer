import glob
import json
import gzip


class FilePathDataLoader:
    """Loads data from a specified file path"""
    def __init__(self, path):
        """Creates a data loader for the given file path"""
        self.__path = path

    def load(self):
        """Loads data from all files in the file path"""
        chunks = []
        for file in glob.glob(self.__path):
            print(f"Loading {file}...")
            chunks += [json.loads(line.strip()) for line in gzip.open(file, 'r')]

        return chunks;