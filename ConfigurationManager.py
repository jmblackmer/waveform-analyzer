import json


class ConfigurationManager:
    """Loads and stores configuration data for easier retrieval"""
    def __init__(self, file='config.json'):
        self.__config = ConfigurationManager.load(file)

    @staticmethod
    def load(file):
        """Loads a configuration from a json file"""
        with open(file, 'r') as f:
            return json.load(f)

    def get_value(self, key, default=None):
        """Returns a value from the config. Returns default if value doesn't exist in config."""
        return self.__config[key] if key in self.__config else default
