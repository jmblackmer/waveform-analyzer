from setuptools import setup

setup(
    install_requires=[
        "numpy",
        "matplotlib",
        "flask",
        "cryptography"
    ]
)