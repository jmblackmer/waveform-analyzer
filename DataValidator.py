class DataValidator:
    """Validates that the data provided is valid data"""
    def __init__(self, min_value, max_value, frequency):
        self.__min_value = min_value
        self.__max_value = max_value
        self.__frequency = frequency

    def validate_chunk(self, chunk):
        """Ensures that a chunk is valid"""
        if len(chunk['values']) != self.__frequency:
            print(f"Chunk does not contain {self.__frequency} values:")
            print(chunk)
            return False

        if chunk['time'] % 250 != 0:
            print(f"Timestamp ({chunk['time']}) is not an even quarter second")
            return False

        for value in chunk['values']:
            if not self.validate_value(value):
                return False

        return True

    def validate_value(self, value):
        """Ensures that a value from a chunk is valid"""
        if value < self.__min_value or value > self.__max_value:
            print(f"Value ({value}) out of range")
            return False

        return True
