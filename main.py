import numpy
import json
import csv
import cryptography
from ConfigurationManager import ConfigurationManager
from WaveformRepository import WaveformRepository
from DataValidator import DataValidator
from DataLoader import FilePathDataLoader
from matplotlib import pyplot
from flask import Flask, url_for, request

# combine configuration with assumptions for when the config values are empty
configuration_manager = ConfigurationManager("config.json")
int16 = numpy.iinfo(numpy.int16)
min_value = configuration_manager.get_value("minValue", int16.min)
max_value = configuration_manager.get_value("maxValue", int16.max)
frequency = configuration_manager.get_value("frequency", 240)
hours_offset = configuration_manager.get_value("hoursOffset", 1.75)
duration_in_minutes = configuration_manager.get_value("durationInMinutes", 10)
path = configuration_manager.get_value("dataPath")

# create repository to store data
data_loader = FilePathDataLoader(path)
validator = DataValidator(min_value, max_value, frequency)
repository = WaveformRepository(data_loader, validator, frequency)

# Start analysis. Pull data from pre-selected range and filter good data.
chart_data = repository.get_range(hours_offset, duration_in_minutes)
good_data = [d for d in chart_data if d is not None]

# Run basic stats on data
stats = [['mean', numpy.mean(good_data) if len(good_data) > 0 else 0],
         ['max', numpy.amax(good_data) if len(good_data) > 0 else 0],
         ['min', numpy.amin(good_data) if len(good_data) > 0 else 0],
         ['std dev', numpy.std(good_data) if len(good_data) > 0 else 0]]

# Format rows of table
labels = [stat[0] for stat in stats]
formatted_data = [["%f" % stat[1] for stat in stats]]

# Create plots and table
fig, axs = pyplot.subplots(2)
axs[0].set_title('Missing Data Included')
axs[0].plot(chart_data)
axs[1].set_title('Missing Data Excluded')
axs[1].plot(good_data)
pyplot.subplots_adjust(hspace=0.5)
pyplot.table(colLabels=labels,
             cellText=formatted_data,
             loc='bottom',
             cellLoc='center',
             rowLoc='center',
             bbox=[0.1, -0.6, 0.8, 0.4])
pyplot.subplots_adjust(bottom=0.2)

# Save as PDF
pyplot.savefig('waveform.pdf')
# pyplot.show()

# Create CSV
with open('chunks.csv', 'w', newline='') as csv_file:
    writer = csv.writer(csv_file)
    for chunk in data_loader.load():
        writer.writerow([chunk['time'],
                         len(chunk['values']),
                         numpy.mean(chunk['values']),
                         numpy.std(chunk['values']),
                         numpy.amin(chunk['values']),
                         numpy.amax(chunk['values'])])

    # Start HTTP server
http = Flask("WaveformHTTP", static_folder="static", static_url_path="/static")


@http.route('/')
def index():
    return http.send_static_file("waveform.html")


# API endpoint for requesting waveform data
@http.route("/api/waveform", methods=['GET'])
def waveform():
    offset = float(request.args.get('hoursOffset'))
    duration = float(request.args.get('durationInMinutes'))
    data = repository.get_range(offset, duration)
    times = repository.get_timestamps(offset, duration)
    good_data = [d for d in data if d is not None]
    result = {
        "data": data,
        "time": times
    }
    return json.dumps(result)


# Start server
http.run(host="127.0.0.1", port=8080, ssl_context='adhoc')
