(function($) {
    var setStat = function($table, stat, value) {
        var statContainer = $table.find("." + stat + "-container");
        statContainer.html("<span>" + value + "</span>");
    }

    function waveform(id) {
        var $container = $(id);
        $container.width(screen.width * .6);
        var canvasElement = $("<canvas class='waveform'></canvas>")[0];
        var $table = $(
            "<table class='stats'>" +
                "<thead>" +
                    "<tr>" +
                        "<th>Min</th>" +
                        "<th>Max</th>" +
                        "<th>Mean</th>" +
                        "<th>Standard Deviation</th>" +
                    "</tr>" +
                "</thead>" +
                "<tbody>" +
                    "<tr>" +
                        "<td class='min-container'></td>" +
                        "<td class='max-container'></td>" +
                        "<td class='mean-container'></td>" +
                        "<td class='stdev-container'></td>" +
                    "</tr>" +
                "</tbody>" +
            "</table>"
        );

        var chart;
        $container.append(canvasElement);
        $container.append($table);

        this.display = function (data) {
            var context = canvasElement.getContext('2d');

            var points = data.map((datum, index) => { return { "x": index, "y": datum }; });

            if (typeof chart === "undefined") {
                chart = new Chart(context, {
                    type: 'scatter',
                    data: {
                        datasets: [{
                            label: 'Waveform',
                            data: points,
                            showLine: true,
                            pointRadius: 0,
                            borderColor: 'rgba(0,39,76,1)',
                            borderWidth: 2,
                            backgroundColor: 'rgba(255,203,5,.8)'
                        }]
                    },
                    options: {
                        legend: { display: false },
                        tooltip: { enabled: false },
                        elements: {
                            line: { tension: 0 }
                        },
                        animation: { duration: 0 },
                        hover: { animationDuration: 0 },
                        responsiveAnimationDuration: 0,
                        scales: {
                            xAxes: [{ display: false }]
                        }
                    }
                });
            } else {
                chart.config.data.datasets[0].data = points;
                chart.update();
            }

            goodData = data.filter(d => typeof d === "number");
            setStat($table, "min", Math.round(math.min(goodData)));
            setStat($table, "max", Math.round(math.max(goodData)));
            setStat($table, "mean", Math.round(math.mean(goodData)));
            setStat($table, "stdev", Math.round(math.std(goodData)));
        }
    }

    $.extend({
        waveform: function (id) {
            return new waveform(id);
        }
    });
})(jQuery);